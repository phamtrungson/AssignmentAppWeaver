﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using AppWeaver.Common.Common;
using AppWeaver.WebApp.Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AppWeaver.WebApp.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Snippet> Snippets { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public ApplicationDbContext()
            : base(Configurations.AppWeaverConnectionString, throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}