﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AppWeaver.Common.Domain;

namespace AppWeaver.WebApp.Domain
{
    public class Snippet : BaseEntity
    {
        [StringLength(255)]
        [Required]
        public string Title { get; set; }

        public string Code { get; set; }

        public string Note { get; set; }

        public IList<Tag> Tags { get; set; }

        [Required]
        public ApplicationUser User { get; set; }

        public Snippet()
        {
            Tags = new List<Tag>();
        }
    }
}
