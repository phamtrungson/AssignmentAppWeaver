﻿using System.ComponentModel.DataAnnotations;
using AppWeaver.Common.Domain;

namespace AppWeaver.WebApp.Domain
{
    public class Tag : BaseEntity
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public Snippet Snippet { get; set; }
    }
}
