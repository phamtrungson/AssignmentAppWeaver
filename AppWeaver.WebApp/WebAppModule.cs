﻿using AppWeaver.WebApp.Service;
using Ninject.Modules;

namespace AppWeaver.WebApp
{
    public class WebAppModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>().InSingletonScope();
            Bind<ISnippetService>().To<SnippetService>().InSingletonScope();
        }
    }
}
