﻿using System.Web.Mvc;
using AppWeaver.WebApp.Service;
using Microsoft.AspNet.Identity;

namespace AppWeaver.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        public HomeController(IUserService userService)
        {
            _userService = userService;
        }
        public ActionResult Index()
        {
            ViewBag.Snippets = _userService.GetUser(User.Identity.GetUserId(), x => x.Snippets).Snippets;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}