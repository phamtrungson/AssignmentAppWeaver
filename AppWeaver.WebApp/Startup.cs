﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppWeaver.WebApp.Startup))]
namespace AppWeaver.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
