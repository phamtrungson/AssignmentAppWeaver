﻿using System;
using System.Linq.Expressions;
using AppWeaver.WebApp.Domain;

namespace AppWeaver.WebApp.Service
{
    public interface IUserService
    {
        ApplicationUser GetUser(string id, params Expression<Func<ApplicationUser, object>>[] paths);
    }
}
