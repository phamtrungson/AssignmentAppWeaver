﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using AppWeaver.WebApp.Domain;
using AppWeaver.WebApp.Models;

namespace AppWeaver.WebApp.Service
{
    public class UserService : IUserService
    {
        public UserService()
        {
        }

        public ApplicationUser GetUser(string id, params Expression<Func<ApplicationUser, object>>[] paths)
        {
            using (var context = ApplicationDbContext.Create())
            {
                var query = context.Users.AsQueryable();
                if (paths != null && paths.Length > 0)
                {
                    paths.Aggregate(query, (q, path) => q.Include(path));
                }
                Expression<Func<ApplicationUser, bool>> filter = x => x.Id == id;
                return query.FirstOrDefault(filter);
            }
        }
    }
}
