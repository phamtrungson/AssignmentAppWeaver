﻿using System.Collections.Generic;
using AppWeaver.WebApp.Domain;
using AppWeaver.WebApp.Models;

namespace AppWeaver.WebApp.Service
{
    public interface ISnippetService
    {

        Snippet CreateSnippet(CreateSnippetModel model);
    }
}
